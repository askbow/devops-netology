задание https://github.com/netology-code/sysadm-homeworks/blob/devsys10/02-git-02-base/README.md

## Задание №1 – Знакомимся с gitlab и bitbucket 

1. Создадим аккаунт в gitlab, если у вас его еще нет: ♻✅ https://gitlab.com/askbow/devops-netology
2. Теперь необходимо проделать все тоже самое с bitbucket ♻✅ https://bitbucket.org/dborchev/devops-netology/src/main/

## Задание №2 – Теги

1. Создайте легковестный тег `v0.0` на HEAD коммите и запуште его во все три добавленных на предыдущем этапе `upstream` ✅ 
1. Аналогично создайте аннотированный тег `v0.1` ✅
1. Перейдите на страницу просмотра тегов в гитхабе (и в других репозиториях) и посмотрите, чем отличаются созданные теги. 
    * В гитхабе – https://github.com/dborchev/devops-netology/tags
    * В гитлабе – https://gitlab.com/askbow/devops-netology/-/tags
    * В битбакете – https://bitbucket.org/dborchev/devops-netology/commits/tag/v0.1